package ru.majestic.tcp;

import java.net.Socket;

public interface ITCPSocketReader {

	void setSocket(Socket socket);
	void startRead();
	
	void setTCPSocketReaderListener(TCPSocketReaderListener tcpSocketReaderListener);
	
	interface TCPSocketReaderListener {
		
		void onTCPMessageReceived(String message);
		void onTCPReadError(String errorInfo);
		
	}
}
