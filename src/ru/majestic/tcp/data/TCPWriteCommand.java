package ru.majestic.tcp.data;

public class TCPWriteCommand {
	
	private final long createTime;
	private final String command;		
	
	public TCPWriteCommand(long createTime, String command) {
		this.createTime = createTime;
		this.command = command;
	}
	
	public long getCreateTime() {
		return createTime;
	}
	
	public String getCommand() {
		return command;
	}

}
