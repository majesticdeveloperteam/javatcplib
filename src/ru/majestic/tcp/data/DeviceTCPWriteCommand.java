package ru.majestic.tcp.data;

public class DeviceTCPWriteCommand {
	
	private final long createTime;
	private final byte[] command;		
	
	public DeviceTCPWriteCommand(long createTime, byte[] command) {
		this.createTime = createTime;
		this.command = command;
	}
	
	public long getCreateTime() {
		return createTime;
	}
	
	public byte[] getCommand() {
		return command;
	}

}
