package ru.majestic.tcp;

import java.net.Socket;

public interface ITCPSocketWriter {

	void setSocket(Socket socket);
	void writeCommand(String command);
	
	void setTCPSocketWriterListener(TCPSocketWriterListener tcpSocketWriterListener);
	
	interface TCPSocketWriterListener {
		
		void onTCPCommandWrote(String command);
		void onTCPCommandWriteError(String errorInfo);
		
	}
	
}
