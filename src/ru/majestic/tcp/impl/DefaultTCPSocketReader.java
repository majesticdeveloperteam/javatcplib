package ru.majestic.tcp.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import ru.majestic.tcp.ITCPSocketReader;

public class DefaultTCPSocketReader implements ITCPSocketReader, Runnable {

	protected TCPSocketReaderListener tcpSocketReaderListener;
	protected Socket socket;
	
	private Thread readTask;
	
	@Override
	public void setSocket(Socket socket) {
		this.socket = socket;
	}
	
	@Override
	public void startRead() {
		if(socket != null) {
			readTask = new Thread(this);
			readTask.start();
		} else {
			if(tcpSocketReaderListener != null)
				tcpSocketReaderListener.onTCPReadError("Client socket is null");
		}
	}

	@Override
	public void setTCPSocketReaderListener(TCPSocketReaderListener tcpSocketReaderListener) {
		this.tcpSocketReaderListener = tcpSocketReaderListener;		
	}
	
	@Override
	public void run() {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String command = null;

			while ((command = br.readLine()) != null) {

				if (tcpSocketReaderListener != null)
					tcpSocketReaderListener.onTCPMessageReceived(command);
			}
			if(tcpSocketReaderListener != null)
				tcpSocketReaderListener.onTCPReadError("Input stream closed");
			
		} catch (IOException e) {
			if(tcpSocketReaderListener != null)
				tcpSocketReaderListener.onTCPReadError(e.toString());
		}
	}	
}
