package ru.majestic.tcp.impl;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.PriorityQueue;
import java.util.Queue;

import ru.majestic.tcp.ITCPSocketWriter;
import ru.majestic.tcp.data.TCPWriteCommand;
import ru.majestic.tcp.utils.TCPCommandsQueueComparator;

public class DefaultTCPSocketWriter implements ITCPSocketWriter, Runnable {

	private static final int WRITE_COMMAND_TRIES_BEFORE_ERROR = 5;
	
	private static final int QUEUE_MAX_CAPACITY = 1000;
	
	private Queue<TCPWriteCommand> commands = new PriorityQueue<TCPWriteCommand>(QUEUE_MAX_CAPACITY, new TCPCommandsQueueComparator());
	
	private Socket socket;
	private Thread writeTask;
	
	private TCPSocketWriterListener tcpSocketWriterListener;
	
	private int writeCommandTries;
	
	public DefaultTCPSocketWriter() {
		writeCommandTries = 0;
	}
	
	@Override
	public void setSocket(Socket socket) {
		this.socket = socket;		
	}
	
	@Override
	public void writeCommand(String command) {
		
		synchronized (commands) {
			if(!commands.offer(new TCPWriteCommand(System.nanoTime(), command))) {
				System.err.println("[ERROR] Add command in queue error: " + command);
			}
		}		
		
		if(writeTask == null) {
			writeTask = new Thread(this);
			writeTask.start();
		}
	}

	@Override
	public void setTCPSocketWriterListener(TCPSocketWriterListener tcpSocketWriterListener) {		
		this.tcpSocketWriterListener = tcpSocketWriterListener;
	}

	@Override
	public void run() {
		synchronized (commands) {
			try {
				BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
				
				TCPWriteCommand command = null;
				
				while((command = commands.poll()) != null) {
					tryToSendCommand(bw, command.getCommand());					
				}
				
			} catch (IOException e) {
				if(tcpSocketWriterListener != null)
					tcpSocketWriterListener.onTCPCommandWriteError(e.toString());
			} finally {
				writeTask = null;
			}
		}
	}	
	
	//===== <PRIVATE_METHODS> =====
	private void tryToSendCommand(BufferedWriter bw, String command) throws IOException {
		try {
			bw.write(command + "\r\n");
			bw.flush();
			
			writeCommandTries = 0;
			
			if(tcpSocketWriterListener != null)
				tcpSocketWriterListener.onTCPCommandWrote(command);
			
		} catch (IOException e) {
			writeCommandTries++;
			if(writeCommandTries >= WRITE_COMMAND_TRIES_BEFORE_ERROR) {
				throw new IOException(e);
			} else {
				tryToSendCommand(bw, command);
			}
		}
	}
	//===== </PRIVATE_METHODS> =====	

}
