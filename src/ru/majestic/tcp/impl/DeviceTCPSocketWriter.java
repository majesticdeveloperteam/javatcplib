package ru.majestic.tcp.impl;

import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.util.PriorityQueue;
import java.util.Queue;

import ru.majestic.tcp.ITCPSocketWriter;
import ru.majestic.tcp.data.DeviceTCPWriteCommand;
import ru.majestic.tcp.utils.DeviceTCPCommandsQueueComparator;

public class DeviceTCPSocketWriter implements ITCPSocketWriter, Runnable {

	private static final int WRITE_COMMAND_TRIES_BEFORE_ERROR = 5;
	
	private static final int QUEUE_MAX_CAPACITY = 1000;
	
	private Queue<DeviceTCPWriteCommand> commands = new PriorityQueue<DeviceTCPWriteCommand>(QUEUE_MAX_CAPACITY, new DeviceTCPCommandsQueueComparator());
	
	private Socket socket;
	private Thread writeTask;
	
	private TCPSocketWriterListener tcpSocketWriterListener;
	
	private int writeCommandTries;
	
	public DeviceTCPSocketWriter() {
		writeCommandTries = 0;
	}
	
	@Override
	public void setSocket(Socket socket) {
		this.socket = socket;		
	}
	
	@Override
	public void writeCommand(String command) {		
		synchronized (commands) {
			if(!commands.offer(new DeviceTCPWriteCommand(System.nanoTime(), command.getBytes()))) {
				System.err.println("[ERROR] Add command in queue error: " + command);
			}
		}		
		
		if(writeTask == null) {
			writeTask = new Thread(this);
			writeTask.start();
		}
	}

	@Override
	public void setTCPSocketWriterListener(TCPSocketWriterListener tcpSocketWriterListener) {		
		this.tcpSocketWriterListener = tcpSocketWriterListener;
	}

	@Override
	public void run() {
		synchronized (commands) {
			try {
				DeviceTCPWriteCommand command = null;
				
				while((command = commands.poll()) != null) {
					tryToSendCommand(socket.getOutputStream(), command.getCommand());					
				}
				
			} catch (IOException e) {
				if(tcpSocketWriterListener != null)
					tcpSocketWriterListener.onTCPCommandWriteError(e.toString());
			} finally {
				writeTask = null;
			}
		}
	}	
	
	//===== <PRIVATE_METHODS> =====
	private void tryToSendCommand(OutputStream os, byte[] command) throws IOException {				
		try {
			os.write(command);
//			os.write("\r\n".getBytes());
			os.flush();
			
			writeCommandTries = 0;
			
			if(tcpSocketWriterListener != null)
				tcpSocketWriterListener.onTCPCommandWrote(new String(command));
			
		} catch (IOException e) {
			writeCommandTries++;
			if(writeCommandTries >= WRITE_COMMAND_TRIES_BEFORE_ERROR) {
				throw new IOException(e);
			} else {
				tryToSendCommand(os, command);
			}
		}
	}
	
	private String byteArrayToHex(byte[] a) {
		StringBuilder sb = new StringBuilder(a.length * 2);
		for (byte b : a)
			sb.append(String.format("%02x", b & 0xff) + " ");
		return sb.toString();
	}
	//===== </PRIVATE_METHODS> =====
}
