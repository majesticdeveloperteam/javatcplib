package ru.majestic.tcp.impl;

import java.io.IOException;
import java.net.Socket;

import ru.majestic.tcp.ITCPSocketReader;

public class DeviceTCPSocketReader implements ITCPSocketReader, Runnable {

	protected TCPSocketReaderListener tcpSocketReaderListener;
	protected Socket socket;
	
	private Thread readTask;
	
	@Override
	public void setSocket(Socket socket) {
		this.socket = socket;
	}
	
	@Override
	public void startRead() {
		if(socket != null) {
			readTask = new Thread(this);
			readTask.start();
		} else {
			if(tcpSocketReaderListener != null)
				tcpSocketReaderListener.onTCPReadError("Client socket is null");
		}
	}

	@Override
	public void setTCPSocketReaderListener(TCPSocketReaderListener tcpSocketReaderListener) {
		this.tcpSocketReaderListener = tcpSocketReaderListener;		
	}
	
	@Override
	public void run() {
		try {
			while(true) {
				if(socket.getInputStream().available() > 0) {
					byte[] buffer = new byte[socket.getInputStream().available()];
					socket.getInputStream().read(buffer);
					
					if (tcpSocketReaderListener != null)
						tcpSocketReaderListener.onTCPMessageReceived(new String(buffer));
				}								
			}
			
		} catch (IOException e) {
			if(tcpSocketReaderListener != null)
				tcpSocketReaderListener.onTCPReadError(e.toString());
		}
	}	
}
