package ru.majestic.tcp.utils;

import ru.majestic.tcp.server.ITCPServerClient;

public class TCPClientPinger implements Runnable {

	private static final int PING_TIMEOUT = 5000;
	
	private ITCPServerClient client;
	
	private Thread workingThread;
	
	public void setTCPClient(ITCPServerClient client) {
		this.client = client;
	}
	
	public void start() {
		if(client == null) {
			System.err.println("Start pinger error. Client is null");
			return;
		}
					
		if(workingThread == null) {
			workingThread = new Thread(this);
			workingThread.start();
		} else {
			System.err.println("Start pinger for client " + client.getID() + " error. Pinger already started!");
		}
	}
	
	public void stop() {
		if(workingThread != null) {
			workingThread.interrupt();
			workingThread = null;
		}
	}
	
	@Override
	public void run() {
		while(true) {
			client.writeCommandToClient("ping");
			
			try {
				Thread.sleep(PING_TIMEOUT);
			} catch (InterruptedException e){
				break;
			}
		}
		
		workingThread = null;
	}

}
