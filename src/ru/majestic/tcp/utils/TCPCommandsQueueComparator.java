package ru.majestic.tcp.utils;

import java.util.Comparator;

import ru.majestic.tcp.data.TCPWriteCommand;

public class TCPCommandsQueueComparator implements Comparator<TCPWriteCommand>{

	@Override
	public int compare(TCPWriteCommand arg0, TCPWriteCommand arg1) {

		if(arg0.getCreateTime() > arg1.getCreateTime()) 
			return 1;
		
		else if(arg0.getCreateTime() < arg1.getCreateTime())
			return -1;
		
		else
			return 0;
	}

}
