package ru.majestic.tcp.utils;

import java.util.Comparator;

import ru.majestic.tcp.data.DeviceTCPWriteCommand;

public class DeviceTCPCommandsQueueComparator implements Comparator<DeviceTCPWriteCommand>{

	@Override
	public int compare(DeviceTCPWriteCommand arg0, DeviceTCPWriteCommand arg1) {

		if(arg0.getCreateTime() > arg1.getCreateTime()) 
			return 1;
		
		else if(arg0.getCreateTime() < arg1.getCreateTime())
			return -1;
		
		else
			return 0;
	}

}
