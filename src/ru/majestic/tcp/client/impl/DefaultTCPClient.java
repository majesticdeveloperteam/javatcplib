package ru.majestic.tcp.client.impl;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import ru.majestic.tcp.ITCPSocketReader;
import ru.majestic.tcp.ITCPSocketWriter;
import ru.majestic.tcp.client.ITCPClient;
import ru.majestic.tcp.impl.DefaultTCPSocketReader;
import ru.majestic.tcp.impl.DefaultTCPSocketWriter;

public class DefaultTCPClient implements ITCPClient, Runnable, ITCPSocketReader.TCPSocketReaderListener, ITCPSocketWriter.TCPSocketWriterListener {

	private static final Logger logger = Logger.getLogger(DefaultTCPClient.class);
	
    private static final int SOCKET_CONNECTION_TIMEOUT  = 5000;
    private static final int SOCKET_SO_TIMEOUT          = 10000;

    private List<TCPDeviceConnectionStateListener> tcpDeviceConnectionStateListeners = new ArrayList<ITCPClient.TCPDeviceConnectionStateListener>();

    private String lastAddress;
    private int lastPort;

    private boolean connected;

    private Socket socket;

    private Thread connectionThread;

    private ITCPSocketReader tcpSocketReader;
    private ITCPSocketWriter tcpSocketWriter;

    private boolean connectionClosedByUser;

    public DefaultTCPClient() {
        tcpSocketReader = new DefaultTCPSocketReader();
        tcpSocketReader.setTCPSocketReaderListener(this);

        tcpSocketWriter = new DefaultTCPSocketWriter();
        tcpSocketWriter.setTCPSocketWriterListener(this);

        connected = false;
    }

    @Override
    public void connect(String address, int port) {
        if(!isConnected()) {
            this.lastAddress = address;
            this.lastPort = port;

            connectionThread = new Thread(this);
            connectionThread.start();
        } else {        	
        	logger.error("Connection error to " + lastAddress + ":" + lastPort + ". Client already connected.");
        	
        	notifyTCPDeviceConnectionStateListenerOnTCPDeviceConnectionError();
        }
    }

    @Override
    public void disconnect() {
        connectionClosedByUser = true;               

        closeConnection();
        
        logger.info("Disconected.");
    }
    
    @Override
	public void write(String message) {
    	tcpSocketWriter.writeCommand(message);		
	}

    @Override
    public boolean isConnected() {
        return connected;
    }

    @Override
    public void addTCPDeviceConnectionStateListener(TCPDeviceConnectionStateListener tcpDeviceConnectionStateListener) {
    	tcpDeviceConnectionStateListeners.add(tcpDeviceConnectionStateListener);
    }
    
    @Override
    public void removeTCPDeviceConnectionStateListener(TCPDeviceConnectionStateListener tcpDeviceConnectionStateListener) {
    	tcpDeviceConnectionStateListeners.remove(tcpDeviceConnectionStateListener);
    }

    @Override
    public void run() {
        try {
            socket = new Socket();
            socket.connect(new InetSocketAddress(lastAddress, lastPort), SOCKET_CONNECTION_TIMEOUT);
            socket.setSoTimeout(SOCKET_SO_TIMEOUT);

            tcpSocketReader.setSocket(socket);
            tcpSocketReader.startRead();

            tcpSocketWriter.setSocket(socket);

            connectionClosedByUser = false;

            connected = true;
            
            logger.info("Connection success to " + lastAddress + ":" + lastPort);

            notifyTCPDeviceConnectionStateListenerOnTCPDeviceConnected(lastAddress, lastPort);

        } catch (IOException e) {
        	logger.error("Connection error to " + lastAddress + ":" + lastPort + ". Reason: " + e.toString());
        	
            socket = null;

            notifyTCPDeviceConnectionStateListenerOnTCPDeviceConnectionError();
        }
    }

    @Override
    public void onTCPMessageReceived(String message) {    	    	
        if(message.equals("ping")) {
        	logger.debug("<-- " + message);
        	
            tcpSocketWriter.writeCommand("ping");

        } else {
        	logger.info("<-- " + message);
        	
        	notifyTCPDeviceConnectionStateListenerOnMessageRecevied(message);            
        }
    }

    @Override
    public void onTCPReadError(String errorInfo) {
        if(!connectionClosedByUser) {        	        	
            closeConnection();
            
            logger.error("Connection lost. Read error.");

            notifyTCPDeviceConnectionStateListenerOnTCPDeviceConnectionLost();
        }
    }

    @Override
    public void onTCPCommandWrote(String command) {
    	if(command.equals("ping")) {
        	logger.debug("--> " + command);

        } else {
        	logger.info("<-- " + command);
        	
        	//.
        }
    }

    @Override
    public void onTCPCommandWriteError(String errorInfo) {
        if(!connectionClosedByUser) {        	        	
            closeConnection();
            
            logger.error("Connection lost. Write error.");

            notifyTCPDeviceConnectionStateListenerOnTCPDeviceConnectionLost();
        }
    }

    //===== <PRIVATE_METHODS> =====
    private void closeConnection() {
        try {
            if(socket != null) {
                socket.shutdownInput();
                socket.shutdownOutput();
                socket.close();
            }
        } catch (IOException e) {

        } finally {
            socket = null;

            connected = false;

            if(connectionClosedByUser) {
            	notifyTCPDeviceConnectionStateListenerOnTCPDeviceDisconnected();
            }
        }
    }
    
    private void notifyTCPDeviceConnectionStateListenerOnMessageRecevied(String message) {
    	for(TCPDeviceConnectionStateListener listener: tcpDeviceConnectionStateListeners) {
    		listener.onMessageReceived(message);
    	}
    }
    
    private void notifyTCPDeviceConnectionStateListenerOnTCPDeviceConnected(String address, int port) {
    	for(TCPDeviceConnectionStateListener listener: tcpDeviceConnectionStateListeners) {
    		listener.onTCPDeviceConnected(address, port);
    	}
    }
    
    private void notifyTCPDeviceConnectionStateListenerOnTCPDeviceConnectionError() {
    	for(TCPDeviceConnectionStateListener listener: tcpDeviceConnectionStateListeners) {
    		listener.onTCPDeviceConnectionError();
    	}
    }
    
    private void notifyTCPDeviceConnectionStateListenerOnTCPDeviceConnectionLost() {
    	for(TCPDeviceConnectionStateListener listener: tcpDeviceConnectionStateListeners) {
    		listener.onTCPDeviceConnectionLost();
    	}
    }
    
    private void notifyTCPDeviceConnectionStateListenerOnTCPDeviceDisconnected() {
    	for(TCPDeviceConnectionStateListener listener: tcpDeviceConnectionStateListeners) {
    		listener.onTCPDeviceDisconnected();
    	}
    }
    //===== </PRIVATE_METHODS> =====	
}
