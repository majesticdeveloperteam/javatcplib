package ru.majestic.tcp.client;

public interface ITCPClient {

    void connect(String address, int port);
    void disconnect();
    boolean isConnected();
    
    void write(String message);

    void addTCPDeviceConnectionStateListener(TCPDeviceConnectionStateListener tcpDeviceConnectionStateListener);
    void removeTCPDeviceConnectionStateListener(TCPDeviceConnectionStateListener tcpDeviceConnectionStateListener);

    interface TCPDeviceConnectionStateListener {

        void onMessageReceived(String message);

        void onTCPDeviceConnected(String address, int port);
        void onTCPDeviceConnectionError();
        void onTCPDeviceConnectionLost();
        void onTCPDeviceDisconnected();

    }
}
