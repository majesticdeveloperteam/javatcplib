package ru.majestic.tcp.server;

import java.net.Socket;

public interface ITCPServerClient {
	
	void setClientSocket(Socket clientSocket);
	
	void	setID(long id);
	long 	getID();
	
	void connect();
	void disconnect();	
	boolean isConnected();
	
	void writeCommandToClient(String command);
	
	void setTCPClientStateListener(TCPClientStateListener tcpClientStateListener);
	
	interface TCPClientStateListener {
	
		void onClientMessageWrote(ITCPServerClient tcpClient, String message);
		void onClientMessageReceived(ITCPServerClient tcpClient, String message);
		void onClientDisconnected(ITCPServerClient client);
		
	}

}
