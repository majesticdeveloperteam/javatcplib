package ru.majestic.tcp.server.impl;

import java.io.IOException;
import java.net.Socket;

import ru.majestic.tcp.ITCPSocketReader;
import ru.majestic.tcp.ITCPSocketWriter;
import ru.majestic.tcp.impl.DefaultTCPSocketReader;
import ru.majestic.tcp.impl.DefaultTCPSocketWriter;
import ru.majestic.tcp.server.ITCPServerClient;
import ru.majestic.tcp.utils.TCPClientPinger;


public class DefaultTCPServerClient implements ITCPServerClient, 
										 ITCPSocketReader.TCPSocketReaderListener,
										 ITCPSocketWriter.TCPSocketWriterListener {

	protected Socket clientSocket;
	
	protected ITCPSocketReader tcpClientSocketReader; 
	protected ITCPSocketWriter tcpClientSocketWriter;
	
	private TCPClientStateListener tcpClientStateListener;
	
	protected TCPClientPinger tcpClientPinger;
	
	private long id;
	
	protected boolean connected;
	
	public DefaultTCPServerClient() {
		this.id = 0L;
		this.connected = false;
		
		tcpClientPinger = new TCPClientPinger();
	}	
	
	@Override
	public void setClientSocket(Socket clientSocket) {
		this.clientSocket = clientSocket;		
	}
	
	@Override
	public void setID(long id) {
		this.id = id;		
	}

	@Override
	public long getID() {
		return id;
	}
	
	@Override
	public void connect() {
		tcpClientSocketReader = new DefaultTCPSocketReader();
		tcpClientSocketReader.setTCPSocketReaderListener(this);
		tcpClientSocketReader.setSocket(clientSocket);
		tcpClientSocketReader.startRead();	
		
		tcpClientSocketWriter = new DefaultTCPSocketWriter();		 
		tcpClientSocketWriter.setTCPSocketWriterListener(this);		 
		tcpClientSocketWriter.setSocket(clientSocket);
		
		tcpClientPinger.setTCPClient(this);
		tcpClientPinger.start();
		
		connected = true;
	}

	@Override
	public void disconnect() {
		try {
			tcpClientPinger.stop();
			
			clientSocket.shutdownInput();
			clientSocket.shutdownOutput();
			clientSocket.close();
		} catch (IOException e) {}
	}

	@Override
	public void writeCommandToClient(String command) {
		tcpClientSocketWriter.writeCommand(command);
	}

	@Override
	public void setTCPClientStateListener(TCPClientStateListener tcpClientStateListener) {
		this.tcpClientStateListener = tcpClientStateListener;		
	}	
	
	@Override
	public void onTCPReadError(String errorInfo) {
		if(connected) {
			connected = false;
			
			if(tcpClientStateListener != null)
				tcpClientStateListener.onClientDisconnected(this);
		}
	}

	@Override
	public void onTCPMessageReceived(String message) {
		if(tcpClientStateListener != null)
			tcpClientStateListener.onClientMessageReceived(this, message);										
	}

	@Override
	public void onTCPCommandWrote(String command) {
		if(tcpClientStateListener != null)
			tcpClientStateListener.onClientMessageWrote(this, command);										
	}

	@Override
	public void onTCPCommandWriteError(String errorInfo) {
		if(connected) {
			connected = false;
			
			if(tcpClientStateListener != null) {
				tcpClientStateListener.onClientDisconnected(this);
			}				
		}
	}

	@Override
	public boolean isConnected() {
		return connected;
	}	

}
