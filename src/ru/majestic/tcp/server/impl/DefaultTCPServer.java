package ru.majestic.tcp.server.impl;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import ru.majestic.tcp.server.ITCPServer;
import ru.majestic.tcp.server.ITCPServerClient;

public class DefaultTCPServer implements Runnable, ITCPServer, ITCPServerClient.TCPClientStateListener {

	private static final Logger logger = Logger.getLogger(DefaultTCPServer.class);
	
	private static final int DEFAULT_SERVER_PORT = 9000;
	
	private static final int CLIENT_SOCKET_SO_TIMEOUT = 10000;
	
	private Map<Long, ITCPServerClient> clients = new HashMap<Long, ITCPServerClient>();
	
	private TCPServerStateListener tcpServerStateListener;
	
	private int serverPort;
	
	private Thread serverSocketThread;
	private ServerSocket serverSocket;
	
	private boolean serverStarted;
	
	private int nextClientID;
	
	public DefaultTCPServer() {
		this.serverPort = DEFAULT_SERVER_PORT;
		this.serverStarted = false;
	}
		
	@Override
	public void setPort(int serverPort) {
		this.serverPort = serverPort;
		
	}
	
	@Override
	public int getPort() {
		return serverPort;
	}
	
	@Override
	public void startListening() {
		if(!isStarted()) {						
			serverSocketThread = new Thread(this);
			serverSocketThread.start();		
		} else {
			logger.warn("Start server error. Server already started.");
			
			if(tcpServerStateListener != null) {
				tcpServerStateListener.onTCPServerStartFailed("Server already started.");
			}
		}
	}
	
	@Override
	public boolean isStarted() {
		return serverStarted;
	}
	
	@Override
	public void stopListening() {
		if(isStarted()) {
			try {
				disconnectAllClients();
				
				if (serverSocket != null) {
					serverSocket.close();
				}			
			} catch (IOException e) {
				System.err.println("[WARNING] Stop server error: " + e.toString());
			}
			
			if(serverSocketThread != null) {
				serverSocketThread.interrupt();
				serverSocketThread = null;				
			}
			serverStarted = false;
		} else {
			logger.warn("Stop server error. Server not started.");
			
			if(tcpServerStateListener != null) {
				tcpServerStateListener.onTCPServerStoppedFailed("Server not started");
			}
		}		
	}

	public void setTCPServerStateListener(TCPServerStateListener tcpServerStateListener) {
		this.tcpServerStateListener = tcpServerStateListener;		
	}
	
	@Override
	public ITCPServerClient getClientByID(long id) {
		return clients.get(id);
	}
	
	@Override
	public List<ITCPServerClient> getConnectedClientsList() {
		return new ArrayList<ITCPServerClient>(clients.values());
	}

	public void run() {
		try {
			serverSocket = new ServerSocket(getPort());
			serverStarted = true;
			nextClientID = 1;
			
			logger.info("Server started on port " + getPort() + " success.");
			
			if(tcpServerStateListener != null)
				tcpServerStateListener.onTCPServerStartSuccess();
			
			while(serverStarted) {
				Socket newClientSocket = serverSocket.accept();												
				newClientSocket.setSoTimeout(CLIENT_SOCKET_SO_TIMEOUT);
				
				ITCPServerClient newClient = new DefaultTCPServerClient();
				newClient.setClientSocket(newClientSocket);
				newClient.setTCPClientStateListener(this);
				newClient.setID(nextClientID++);
				newClient.connect();
				
				clients.put(newClient.getID(), newClient);
				
				logger.info("Client connected. ID: " + newClient.getID());
				
				if(tcpServerStateListener != null) {
					tcpServerStateListener.onNewClientConnected(newClient);
				}														
			}
			
		} catch (IOException e) {
			logger.info("Server stoped.");
			
			if(tcpServerStateListener != null) {
				tcpServerStateListener.onTCPServerStopped();
			}			
		}		
	}
	
	@Override
	public void onClientMessageReceived(ITCPServerClient tcpClient, String message) {
		if(message.equals("ping")) {
			logger.debug("[" + tcpClient.getID() + "] <-- " + message);
		} else {
			logger.info("[" + tcpClient.getID() + "] <-- " + message);
			
			if(tcpServerStateListener != null)
				tcpServerStateListener.onReceivedMessageFromClient(tcpClient, message);
		}						
	}
	
	@Override
	public void onClientMessageWrote(ITCPServerClient tcpClient, String message) {
		if(message.equals("ping")) {
			logger.debug("[" + tcpClient.getID() + "] --> " + message);
		} else {
			logger.info("[" + tcpClient.getID() + "] --> " + message);
			
			if(tcpServerStateListener != null)
				tcpServerStateListener.onMessageWroteToClient(tcpClient, message);
		}					
	}

	@Override
	public void onClientDisconnected(ITCPServerClient client) {		
		client.disconnect();
		clients.remove(client.getID());
		
		logger.info("Client disconnected. ID: " + client.getID());
		
		if(tcpServerStateListener != null) {
			tcpServerStateListener.onClientDisconnected(client);
		}				
	}	
	
	//===== <PRIVATE_METHODS> =====
	private void disconnectAllClients() throws IOException {
		for(Long key: clients.keySet()) {
			clients.get(key).disconnect();
		}
		
		clients.clear();
	}
	//===== </PRIVATE_METHODS> =====			
}
