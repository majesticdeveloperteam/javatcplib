package ru.majestic.tcp.server.impl;

import java.io.IOException;
import java.net.Socket;

import ru.majestic.tcp.ITCPSocketReader;
import ru.majestic.tcp.ITCPSocketWriter;
import ru.majestic.tcp.impl.DeviceTCPSocketReader;
import ru.majestic.tcp.impl.DeviceTCPSocketWriter;
import ru.majestic.tcp.server.ITCPServerClient;


public class DeviceTCPServerClient implements ITCPServerClient, 
										 ITCPSocketReader.TCPSocketReaderListener,
										 ITCPSocketWriter.TCPSocketWriterListener {

	protected Socket clientSocket;
	
	protected ITCPSocketReader tcpClientSocketReader; 
	protected ITCPSocketWriter tcpClientSocketWriter;
	
	private TCPClientStateListener tcpClientStateListener;
	
	private long id;
	
	protected boolean connected;
	
	public DeviceTCPServerClient() {
		this.id = 0L;
		this.connected = false;
	}	
	
	@Override
	public void setClientSocket(Socket clientSocket) {
		this.clientSocket = clientSocket;		
	}
	
	@Override
	public void setID(long id) {
		this.id = id;		
	}

	@Override
	public long getID() {
		return id;
	}
	
	@Override
	public void connect() {
		tcpClientSocketReader = new DeviceTCPSocketReader();
		tcpClientSocketReader.setTCPSocketReaderListener(this);
		tcpClientSocketReader.setSocket(clientSocket);
		tcpClientSocketReader.startRead();	
		
		tcpClientSocketWriter = new DeviceTCPSocketWriter();		 
		tcpClientSocketWriter.setTCPSocketWriterListener(this);		 
		tcpClientSocketWriter.setSocket(clientSocket);
		
		connected = true;
	}

	@Override
	public void disconnect() {
		try {
			clientSocket.shutdownInput();
			clientSocket.shutdownOutput();
			clientSocket.close();
		} catch (IOException e) {}
	}

	@Override
	public void writeCommandToClient(String command) {
		tcpClientSocketWriter.writeCommand(command);
	}

	@Override
	public void setTCPClientStateListener(TCPClientStateListener tcpClientStateListener) {
		this.tcpClientStateListener = tcpClientStateListener;		
	}	
	
	@Override
	public void onTCPReadError(String errorInfo) {
		if(connected) {
			connected = false;
			
			if(tcpClientStateListener != null)
				tcpClientStateListener.onClientDisconnected(this);
		}
	}

	@Override
	public void onTCPMessageReceived(String message) {
		if(tcpClientStateListener != null)
			tcpClientStateListener.onClientMessageReceived(this, message);										
	}

	@Override
	public void onTCPCommandWrote(String command) {
		if(tcpClientStateListener != null)
			tcpClientStateListener.onClientMessageWrote(this, command);										
	}

	@Override
	public void onTCPCommandWriteError(String errorInfo) {
		if(connected) {
			connected = false;
			
			if(tcpClientStateListener != null) {
				tcpClientStateListener.onClientDisconnected(this);
			}				
		}
	}

	@Override
	public boolean isConnected() {
		return connected;
	}			
}
