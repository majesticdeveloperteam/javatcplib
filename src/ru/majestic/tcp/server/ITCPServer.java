package ru.majestic.tcp.server;

import java.util.List;


public interface ITCPServer {

	public void setPort(int serverPort);
	public int getPort();
	
	public boolean isStarted();
	public void startListening();
	public void stopListening();
	
	public ITCPServerClient getClientByID(long id);
	public List<ITCPServerClient> getConnectedClientsList();		

	public void setTCPServerStateListener(TCPServerStateListener tcpServerStateListener);
	
	interface TCPServerStateListener {
		
		void onNewClientConnected(ITCPServerClient tcpClient);
		void onClientDisconnected(ITCPServerClient tcpClient);
		
		void onMessageWroteToClient(ITCPServerClient client, String message);
		void onReceivedMessageFromClient(ITCPServerClient client, String message);
		
		void onTCPServerStartSuccess();
		void onTCPServerStartFailed(String info);
		
		void onTCPServerStopped();
		void onTCPServerStoppedFailed(String info);
	}
}
